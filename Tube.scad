// smoothness:
$fn=50;
// diameters (use exact values):
outer=15.9;
inner=10;
// adapter height (without ledge):
height=25;
// ledge width:
ledgew=2.5;
// ledge thickness:
ledget=1;
// tolerances:
tol=0.2;
//höhe beampath
bpheight=51.2;
//aussendurchmesser bp
bpouter=15.90;

difference() {
    union () {
    // additiv
    }
    //subtr
}

// main body:
//beamstop part
rotate(a=[0,180,0]) {
difference() {
    difference() {
        cylinder(height+ledget,d=outer+2*ledgew);
        difference() {
            cylinder(height,d=outer+2*ledgew);
            cylinder(height,d=outer+tol);
        }
        cylinder(height+ledget+0.2,d=inner+tol);
    }

    translate([(outer-inner+ledgew),0,height*0.5]) {
        cube([(outer-inner+ledgew)*0.5+3,1,height+2*ledget+0.2], center=true);
    }
}
}
//beampath_part
translate([0,0,51.2])
rotate(a=[0,180,0]) {
    difference(){
    union () {
difference() {

    // additiv
    
      cylinder(bpheight,d=bpouter);
        cylinder(bpheight,d=inner+tol);
}
//beampath_blende 
     translate([0,0,1])
       difference() {
        cylinder(9,d=bpouter);
        cylinder(9,d=4);
    }    
     //beampath_blende 
     translate([0,0,0])
       difference() {
        cylinder(1,d=bpouter);
        cylinder(1,d=8);
    }    
}    
//magnete
          translate([5,1,0])
           cylinder(8,d=4.1);
//magnete
          translate([-5,1,0])
           cylinder(8,d=4.1);

}

//nase
//           translate([-0.5,7.8,22]) 
//            cube([1,2,3]);  


//beampath_blende
//translate([0,0,20])
//rotate(a=[0,180,0]) {
//difference() {
//        cylinder(9,d=bpouter);
//        cylinder(9,d=4);
//    }

//}
}